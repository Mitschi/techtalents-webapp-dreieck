package com.github.mitschi;

import com.github.mitschi.discount.CardType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;

@Controller
public class RabatteController {


    Logger logger = LoggerFactory.getLogger(com.github.mitschi.RabatteController.class);


    private String codeWord = "SOFTWARE";



    @GetMapping("/discountCheck")
    public String goToDiscount() {
        return "discountCheck";
    }


    @PostMapping("/discountCheck")
    public String checkDiscount(@RequestParam(name = "age", required = false) String str_age,
                                @RequestParam(name = "result", required = false) String str_result,
                                ModelMap model) {
        logger.info("age" + str_age);
        logger.info("result " + str_result);


        HashMap<Integer, CardType> borders = getBorders();



        CardType cd_exp = CardType.valueOf(str_result);

        if (str_age.isEmpty()) {
            model.addAttribute("how", "mit einem leeren Eingabewert");
            if (cd_exp.equals(CardType.FEHLER)) {
                model.addAttribute("letter", codeWord.charAt(5));
                return "discount_success";
            }
            return "discount_error";
        }


        try {
            Integer age = Integer.valueOf(str_age);
            CardType cd_calc = checkCardType(age);

            if(borders.containsKey(age) ) {
                String h = getHow(age);

                if(age==0 || age ==60) {
                    if(cd_exp.equals(borders.get(age)) && !cd_calc.equals(cd_exp)) {
                        // hier ist ein fehler versteckt#

                        char c = getCharForCodeword(age);
                        model.addAttribute("letter", c);
                        model.addAttribute("result", cd_exp); // expected result
                        model.addAttribute("discountType", cd_calc); // calculated result
                        return "discount_errorfound_success";
                    }
                    model.addAttribute("how", h);
                    return "discount_error";
                }else if(cd_calc.equals(cd_exp)){
                    model.addAttribute("how", h);
                    char c = getCharForCodeword(age);
                    model.addAttribute("letter", c);
                    String t = getLetterText(age);
                    model.addAttribute("letterText", t);
                    model.addAttribute("result", cd_exp); // expected result
                    model.addAttribute("discountType", cd_calc); // calculated result
                    return "discount_success";
                }
                model.addAttribute("how", h);
                return "discount_error";

              }else {
                // nichts spezielles getestet
                model.addAttribute("result", cd_exp); // expected result
                model.addAttribute("discountType", cd_calc); // calculated result
                return "discountCard";

            }

       } catch (NumberFormatException e) {
            logger.info(e.getMessage());
            try {
                Double a = Double.valueOf(str_age);
                model.addAttribute("how", "mit Dezimalzahlen");

                if (cd_exp.equals(CardType.FEHLER)) {
                    model.addAttribute("letter", codeWord.charAt(6));
                    return "discount_success";
                }
                return "discount_error";

            } catch (NumberFormatException ee) {

                logger.info(ee.getMessage());
                model.addAttribute("how", "mit Text");
                if (cd_exp.equals(CardType.FEHLER)) {
                    model.addAttribute("letter", codeWord.charAt(7));
                    return "discount_success";
                }

                return "discount_error";


            }

        }


    }

    private char getCharForCodeword(int age) {
        if(age==120) {
            return codeWord.charAt(0);
        }else if(age==0) {
            return codeWord.charAt(1);
        }else if(age==18) {
            return codeWord.charAt(2);
        }else if(age==26) {
            return codeWord.charAt(3);
        }else if(age==60) {
            return codeWord.charAt(4);
        }

        return (char)0;
    }

    private String getLetterText(int age) {
        if(age==120 || age == 0 || age == 18 || age == 26 || age ==60) {
            return "Der Buchstabe für das Codwort lautet";
        }
        return "";
    }

    private String getHow(int age) {
        if(age == 120|| age == 121 || age == 119 || age == 0 || age == -1 || age == 1) {
            return "an der Grenze zu den ungültigen Werten mit Alter = "+age;
        }else if(age == 17|| age == 18 || age == 19) {
            return "an der Grenze zwischen Schüler und Student mit Alter = "+age;
        }else if(age == 25|| age == 26 || age == 27) {
            return "an der Grenze zwischen Student und Erwachsener mit Alter = "+age;
        }else if(age == 59|| age == 60 || age == 61) {
            return "an der Grenze zwischen Erwachsener und Pensionist mit Alter = "+age;
        }

        return "";
    }

    private CardType checkCardType(int age){
        if (age >= 0 && age <= 18) {
            // hier ist ein fehler sollte <0 sein!
            return CardType.SCHUELER;
        } else if (age > 18 && age <= 26) {
            return CardType.STUDENT;
        } else if (age > 26 && age < 60) {
            // hier ist ein fehler sollte <=60 sein!
            return CardType.ERWACHSENER;
        } else if (age >= 60 && age <= 120) {
            return CardType.PENSIONIST;
        }else{
            return CardType.FEHLER;
        }
    }
    private HashMap<Integer, CardType> getBorders() {
        HashMap<Integer, CardType> borders = new HashMap<Integer, CardType>();
        borders.put(-1, CardType.FEHLER);
        borders.put(0,CardType.FEHLER);
        borders.put(1,CardType.SCHUELER);
        borders.put(17,CardType.SCHUELER);
        borders.put(18,CardType.SCHUELER);
        borders.put(19,CardType.SCHUELER);
        borders.put(25,CardType.STUDENT);
        borders.put(26,CardType.STUDENT);
        borders.put(27,CardType.ERWACHSENER);
        borders.put(59,CardType.ERWACHSENER);
        borders.put(60,CardType.ERWACHSENER);
        borders.put(61,CardType.PENSIONIST);
        borders.put(119,CardType.PENSIONIST);
        borders.put(120,CardType.PENSIONIST);
        borders.put(121, CardType.FEHLER);
        return borders;
    }


}


