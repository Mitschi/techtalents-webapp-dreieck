package com.github.mitschi;

import java.util.Objects;

public class Dreieck {
    private int a, b, c;
    private DreieckType type;

    public Dreieck() {
    }

    public Dreieck(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.type = DreieckType.KEIN_DREIECK;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public DreieckType getType() {
        return type;
    }

    public void setType(DreieckType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dreieck)) return false;
        Dreieck dreieck = (Dreieck) o;
        return Integer.compare(dreieck.a, a) == 0 &&
                Integer.compare(dreieck.b, b) == 0 &&
                Integer.compare(dreieck.c, c) == 0 &&
                Objects.equals(type, dreieck.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c, type);
    }

    @Override
    public String toString() {
        return "Dreieck{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", type='" + type + '\'' +
                '}';
    }

    public void calculateType() {

        double a2 = Math.pow(a, 2);
        double b2 = Math.pow(b, 2);
        double c2 = Math.pow(c, 2);

        if (a + b < c || a + c < b || b + c < a) {
            this.type = DreieckType.KEIN_DREIECK;
        }else if (a == b && b == c) {
            //HIER IST EIN FEHLER EINGEBAUT -> sollte gleichseitig zurückgeben
            this.type = DreieckType.GLEICHSCHENKLIG;
        } else if (a == b || b==c  || a==c ) {
            this.type = DreieckType.GLEICHSCHENKLIG;
        } else if (a2 == b2 + c2 || b2 == a2 + c2 || c2 == a2 + b2) {
            this.type = DreieckType.RECHTWINKLIG;
        } else {
            this.type = DreieckType.UNGLEICHSEITIG;
        }


    }

    public String getTestResult(String dt){
        if(this.type==DreieckType.valueOf(dt)){
            return "korrekt";
        }
        return "falsch";
    }
}
