package com.github.mitschi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class DreieckController {
    Logger logger = LoggerFactory.getLogger(DreieckController.class);


    private String codeWord = "DREIECK";


    @GetMapping("/dreiecksCheck")
    public String goToTriangle() {

        return "dreiecksCheck";
    }
    @GetMapping("/index")
    public String goToIndex() {

        return "index";
    }


    @PostMapping("/dreiecksCheck")
    public String checkDreieck(@RequestParam(name = "a", required = false) String str_a,
                               @RequestParam(name = "b", required = false) String str_b,
                               @RequestParam(name = "c", required = false) String str_c,
                               @RequestParam(name = "result", required = false) String type,
                               ModelMap model) {
        logger.info("a " + str_a);
        logger.info("b " + str_b);
        logger.info("c " + str_c);
        DreieckType dt_type = DreieckType.valueOf(type);

        if (str_a.isEmpty() || str_b.isEmpty() || str_c.isEmpty()) {

                model.addAttribute("how", "leeren Eingaben");
                if (type.equals("FEHLER")) {
                    model.addAttribute("letter", codeWord.charAt(5));
                    return "result_success";
                }
                return "result_error";
            }

            try {
                Integer a = Integer.valueOf(str_a);
                Integer b = Integer.valueOf(str_b);
                Integer c = Integer.valueOf(str_c);

                if (a > 100 || b > 100 || c > 100) {
                    model.addAttribute("how", "Eingabewerten über 100");
                    if (dt_type.equals(DreieckType.FEHLER)) {
                        model.addAttribute("letter", codeWord.charAt(0));
                        return "result_success";
                    }
                    return "result_error";
                } else if (a == 0 || b == 0 || c == 0) {
                    model.addAttribute("how", "Werten die gleich 0 sind");
                    if (dt_type.equals(DreieckType.FEHLER)) {
                        model.addAttribute("letter", codeWord.charAt(1));
                        return "result_success";
                    }
                    return "result_error";
                } else if (a < 0 || b < 0 || c < 0) {
                    model.addAttribute("how", "negativen Eingabewerten");
                    if (dt_type.equals(DreieckType.FEHLER)) {
                        model.addAttribute("letter", codeWord.charAt(2));
                        return "result_success";
                    }
                    return "result_error";
                }





                Dreieck d = new Dreieck(a, b, c);
                d.calculateType();





                model.addAttribute("result",dt_type.toString()); // expected result


             // redirect for sucessfull detected bug
                if (a == b && b == c ) {

                    if(dt_type.equals(DreieckType.GLEICHSEITIG)){
                        model.addAttribute("dreiecksTyp", d.getType().toString());
                        model.addAttribute("letter", codeWord.charAt(6));
                        return "dreiecksKlasse_success";
                    }else{
                        model.addAttribute("dreiecksTyp", d.getType().toString());
                        return "dreiecksKlasse";
                    }


                }
                model.addAttribute("dreiecksTyp", d.getType().toString());
                //model.addAttribute("isTestTrue",d.getTestResult(type) );
                return "dreiecksKlasse";

            } catch (NumberFormatException e) {
                logger.info(e.getMessage());

                try {
                    Double a = Double.valueOf(str_a);
                    Double b = Double.valueOf(str_b);
                    Double c = Double.valueOf(str_c);


                    model.addAttribute("how", "Dezimalzahlen");

                    if (type.equals("FEHLER")) {
                        model.addAttribute("letter", codeWord.charAt(3));
                        return "result_success";
                    }
                    return "result_error";



                } catch (NumberFormatException ee) {

                    logger.info(ee.getMessage());
                    model.addAttribute("how", "Text");
                    if (type.equals("FEHLER")) {
                        model.addAttribute("letter", codeWord.charAt(4));
                        return "result_success";
                    }

                    return "result_error";


                }

            }



    }

}