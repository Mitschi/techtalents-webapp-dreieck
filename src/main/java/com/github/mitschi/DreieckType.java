package com.github.mitschi;

public enum DreieckType {
    GLEICHSEITIG,
    GLEICHSCHENKLIG,
    RECHTWINKLIG,
    UNGLEICHSEITIG,
    KEIN_DREIECK,
    FEHLER;
}
