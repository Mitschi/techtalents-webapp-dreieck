FROM openjdk:8-jre-alpine

COPY target/techtalents-webapp-dreieck-1.0-SNAPSHOT.jar /app.jar
# run application with this command line
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=default", "/app.jar"]